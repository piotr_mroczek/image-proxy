'use strict';


var express = require('express');
var app = express();

const fs = require('fs');

app.get('/', function (req, res) {
  res.send(':)');
});

app.listen(3000, function () {
  console.log('Running on port 3000 ');

});

function sendImage(dest, res) {

	console.log('wysylam obrazek', dest)


	fs.readFile(dest , (err, data) => {
		
		// if (err) {
		// 	throw err;
		// }	
		
x
		res.setHeader('Cache-Control', 'public, max-age=31557600'); // one year	
		res.end(data);
	}); 


}

function downloadImage(url, dest) {


	console.log('pobieram obrazek ', url, dest)

	const download = require('download');

	return download(url).then(data => {

		console.log('pobralem obrazek ', url, dest, data)
	    fs.writeFile(dest, data);

	});

}


app.get(/getimg\/(.*)/, function (req, res) {
  

	var url = req.params[0];
	const pathizeUrl = require('pathize-url');

	var metdaData = pathizeUrl(url);

	var fileName = 'fetched/' + metdaData.root 
	
	
	var dest = __dirname + '/' + fileName + '/' + metdaData.base

	console.log('Obrazke zrodlowy', dest);

	fs.exists(dest, (exists) => {
  		

		if (exists) {

			sendImage(dest, res);

		} else {


				fs.exists('fetched/' + metdaData.root , (exists) => {
  		
					if (!exists) {

						fs.mkdir('fetched/' + metdaData.root, () => {

								downloadImage(url, dest)
									.then(data => {
										sendImage(dest, res);	
									});	
								
						});
						
					
					} else {

						downloadImage(url, dest)
							.then(data => {

								sendImage(dest, res);	
						});	

					}

				});
			
		}

	});

});	