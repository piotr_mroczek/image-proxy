## What is this?

Simple proxy server for caching images and avoiding of hotlinking to remote server. 


## How to run?

### In old fashioned way

Run 

```
npm install && npm start 

```

to install vendors and start listening on port 3000 

After that You can call url like 

```
http://localhost:3000/getimg/https://git-scm.com/images/logos/logomark-orange@2x.png

```

to download and serve png file for log od GIT project.



### Run Docker image

Run 

```
docker pull piotrmroczek/image-proxy

``` 

and 

```
 docker run -p 3000:3000 piotrmroczek/image-proxy
```
